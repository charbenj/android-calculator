package com.example.androidcalculator

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.StringBuilder


class MainActivity : AppCompatActivity() {

    val str = StringBuilder()
    val operators = listOf("+", "-", "/", "*", ".")
    val history = mutableListOf<String>()
    val numbers = listOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')

    inner class NumberClickListener(private val number: Char) : View.OnClickListener {
        override fun onClick(v: View?) {
            appendNumber(number)
        }
    }

    inner class OperatorClickListener(private val operator: Char, private val number: String) :
        View.OnClickListener {
        override fun onClick(v: View?) {
            appendOperator(operator, number)
            displayText("")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayText("")

        button0.setOnClickListener(NumberClickListener('0'))

        button1.setOnClickListener(NumberClickListener('1'))

        button2.setOnClickListener(NumberClickListener('2'))

        button3.setOnClickListener(NumberClickListener('3'))

        button4.setOnClickListener(NumberClickListener('4'))

        button5.setOnClickListener(NumberClickListener('5'))

        button6.setOnClickListener(NumberClickListener('6'))

        button7.setOnClickListener(NumberClickListener('7'))

        button8.setOnClickListener(NumberClickListener('8'))

        button9.setOnClickListener(NumberClickListener('9'))

        buttonMinus.setOnClickListener(OperatorClickListener('-', displayText.text.toString()))

        buttonPlus.setOnClickListener(OperatorClickListener('+', displayText.text.toString()))

        buttonMultiply.setOnClickListener(OperatorClickListener('*', displayText.text.toString()))

        buttonDivide.setOnClickListener(OperatorClickListener('/', displayText.text.toString()))


        buttonDecimal.setOnClickListener {
            appendNumber('.')
            displayText("")

        }

        buttonPlusMinus.setOnClickListener {
            toggleNegative()
        }


        buttonAC.setOnClickListener {
            history.clear()
            sumHistory.setText("")
            displayText("")
        }

        buttonEquals.setOnClickListener {
            calculateSum()
        }

        buttonBackspace.setOnClickListener {
            // delete sumHistory
            val currentSumHistory = history.toString()
            if (currentSumHistory.length <= 1) {
                history.clear()
            } else if (currentSumHistory.length > 1) {
                val newString = currentSumHistory.substring(0, currentSumHistory.length - 1)
                sumHistory.text = newString
            }
            // delete displayText
            val currentValue = displayText.text.toString()
            if (currentValue.length <= 1) {
                displayText("")
            } else if (currentValue.length > 1) {
                val newString = currentValue.substring(0, currentValue.length - 1)
                displayText(newString)
            }
        }
    }

    private fun toggleNegative() {
        val num = displayText.text.toString()
        if (num.isNotEmpty()) {
            val newNum = num.toInt() * -1
            displayText.setText(newNum.toString())
        }
    }


    fun displayText(text: String) {
        displayText.setText(text)
    }

    private fun appendOperator(operator: Char, number: String) {
        history += number
        history += operator.toString()
        history.toString().replace(",", "", ignoreCase = true)
        log("history=$history")
        sumHistory.setText(history.toString())
    }


    private fun appendNumber(charToBeAdded: Char) {
        displayText.append(charToBeAdded.toString())

    }


    private fun calculateSum() {
        val text = sumHistory.text.toString()
        try {
            val expression = ExpressionBuilder(text).build()
            val result = expression.evaluate()
            val longResult = result.toLong()
            if (result == longResult.toDouble()) {
                displayText.setText(longResult.toString())
            } else {
                displayText.setText(result.toString())
            }
        } catch (ex: IllegalArgumentException) {
            ex.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        log("onStart()")

    }

    override fun onResume() {
        super.onResume()
        log("onResume()")

    }

    override fun onPause() {
        super.onPause()
        log("onPause()")
    }

    override fun onStop() {
        super.onStop()
        log("onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        log("onDestroy()")
    }

    private fun log(message: String) {
        Log.d("ac", message)
    }
}

